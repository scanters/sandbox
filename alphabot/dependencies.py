"""
Dependencies graphs visualizer.
https://graphviz.readthedocs.io/en/stable/manual.html
https://www.graphviz.org/.
"""
from __future__ import annotations

import graphviz


# RANKDIR = 'TB'
RANKDIR = 'LR'
graph = graphviz.Digraph('dependencies', filename='deps.gv')
graph.attr(rankdir=RANKDIR, size='8,5')


# =================================================================
class Node:
    """Library reflection."""

    instances: set[Node] = set()

    def __init__(self, name: str, version: str = 'latest'):
        self.name: str = name
        self.version: str = version
        self.dependencies: set[Node] = set()
        self.instances.add(self)

    def set_dependencies(self, *libs) -> None:
        """Extend dependencies."""
        self.dependencies |= set(libs)
        for dep in self.dependencies:
            graph.edge(str(self), str(dep))

    def __str__(self) -> str:
        return self.name


ALPHABOT_SIM = Node('alphabot_sim')
ALPHA_SIM_INTERFACE = Node('alpha-sim-interface')
BASE64 = Node('base64')
CAPACITOR_BANK_COMMON = Node('capacitor_bank_common')
COMMON_FIRMWARE = Node('common_firmware')
EIGEN = Node('eigen')
HAYAI = Node('hayai')
IPC = Node('ipc')
JSON = Node('json')
LIBALERT = Node('libalert')
LIBUCL = Node('libucl')
LINE_SENSOR_COMMS = Node('line_sensor_comms')
MOTOR_TUNING = Node('motor_tuning')
NETSNPM = Node('netsnmp')
PCU_ICOMM_MSG = Node('pcu_icomm_msg')
RADSM = Node('radsm')
SHELL_BASED_INSTALLER = Node('shell_based_installer')
SLL = Node('sll')
THL = Node('thl')
VIZIER = Node('vizier')
FMT = Node('fmt')
GOOGLETEST = Node('googletest')
ICOMM = Node('icomm')
PROTOBUF_COMPILER = Node('protobuf-compiler')
RUBY_APM_CREATOR = Node('ruby_apm_creator')
SPDLOG = Node('spdlog')
GAZEBO = Node('gazebo')

# === SET DEPS ===
LIBALERT.set_dependencies(BASE64, EIGEN, HAYAI, JSON, LIBUCL, SLL, SPDLOG, VIZIER)
VIZIER.set_dependencies(GOOGLETEST, JSON, SPDLOG, THL)
THL.set_dependencies(FMT, GOOGLETEST)
RADSM.set_dependencies(HAYAI, GOOGLETEST)
SLL.set_dependencies(RUBY_APM_CREATOR)
IPC.set_dependencies(SPDLOG, FMT, GOOGLETEST)
PCU_ICOMM_MSG.set_dependencies(ICOMM)
ICOMM.set_dependencies(GOOGLETEST)
LINE_SENSOR_COMMS.set_dependencies(COMMON_FIRMWARE)
ALPHABOT_SIM.set_dependencies(GAZEBO)
ALPHA_SIM_INTERFACE.set_dependencies(PROTOBUF_COMPILER)

# === MAIN LOGIC ===
TOP_LIBS: list[Node] = [
    ALPHABOT_SIM,
    ALPHA_SIM_INTERFACE,
    BASE64,
    CAPACITOR_BANK_COMMON,
    COMMON_FIRMWARE,
    EIGEN,
    HAYAI,
    IPC,
    JSON,
    LIBALERT,
    LIBUCL,
    LINE_SENSOR_COMMS,
    MOTOR_TUNING,
    NETSNPM,
    PCU_ICOMM_MSG,
    RADSM,
    SHELL_BASED_INSTALLER,
    SLL,
    THL,
    VIZIER,
]

top_app = Node('Alphabot')
top_app.set_dependencies(*TOP_LIBS)

graph.view()
